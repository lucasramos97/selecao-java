import { Component, OnInit } from '@angular/core';

import { MenuItem } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private items: MenuItem[];

  constructor(private router: Router) { }

  ngOnInit() {
    this.items = [
      { label: 'Seleçao Java' },
      { label: 'Combustível CRUD', url: '/combustivel' },
      { label: 'Usuário CRUD' },
      { label: 'Operações com Combustível' },
    ];
  }

  combustivelCRUD() {
    this.router.navigate(['/combustivel']);
  }

  usuarioCRUD() {
    this.router.navigate(['/usuario']);
  }

  operacoesComCombustivel() {
    this.router.navigate(['/combustivel/operacoes']);
  }

}
