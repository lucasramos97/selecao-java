import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { Usuario } from './../model/usuario';
import { RotasService } from './../../rotas.service';
import { UsuarioService } from './../usuario.service';

@Component({
  selector: 'app-ver',
  templateUrl: './ver.component.html',
  styleUrls: ['./ver.component.css']
})
export class VerComponent implements OnInit, OnDestroy {

  private usuario: Usuario;

  private idSubscription: Subscription;
  private usuarioSubscription: Subscription;

  constructor(
    private usuarioService: UsuarioService,
    private activatedRoute: ActivatedRoute,
    private rotasService: RotasService
  ) { }

  ngOnInit() {
    this.idSubscription = this.activatedRoute.params.subscribe(
      params => {
        if (params.id != null) {
          this.usuarioSubscription = this.usuarioService.getById(params.id).subscribe(
            value => this.usuario = value
          );
        }
      }
    );
  }

  listUsuario() {
    this.rotasService.listUsuario();
  }

  addUsuario() {
    this.rotasService.addUsuario();
  }

  editarUsuario(id: number) {
    this.rotasService.editarUsuario(id);
  }

  home() {
    this.rotasService.home();
  }

  ngOnDestroy() {
    if (this.idSubscription != null) {
      this.idSubscription.unsubscribe();
    }
    if (this.usuarioSubscription != null) {
      this.usuarioSubscription.unsubscribe();
    }
  }

}
