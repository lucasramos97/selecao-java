import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';

import { Usuario } from './../model/usuario';
import { UsuarioService } from './../usuario.service';
import { RotasService } from './../../rotas.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit, OnDestroy {

  private usuario: Usuario;
  private formGroup: FormGroup;
  private usuarioID: number;
  private visiblePassword = 'password';

  private idSubscription: Subscription;
  private usuarioSubscription: Subscription;
  private saveUsuarioSubscription: Subscription;
  private updateUsuarioSubscription: Subscription;

  constructor(
    private usuarioService: UsuarioService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private rotasService: RotasService
  ) { }

  ngOnInit() {
    this.idSubscription = this.activatedRoute.params.subscribe(
      params => {
        if (params.id != null) {
          this.usuarioSubscription = this.usuarioService.getById(params.id).subscribe(
            value => {
              this.usuarioID = value.id;
              this.usuario = value;
              this.formGroup = this.formEdit();
          });
        } else {
          this.usuarioID = 0;
          this.formGroup = this.formClear();
        }
      }
    );
  }

  formEdit() {
    return this.formBuilder.group({
      nome: [this.usuario.nome],
      username: [this.usuario.username],
      email: [this.usuario.email],
      numeroDeTelefone: [this.usuario.numeroDeTelefone],
      password: [this.usuario.password]
    });
  }

  formClear() {
    return this.formBuilder.group({
      nome: [null],
      username: [null],
      email: [null],
      numeroDeTelefone: [null],
      password: [null]
    });
  }

  onSubmit() {
    if (this.usuarioID !== 0) {
      this.updateUsuarioSubscription = this.usuarioService
      .update(this.getUsuario(this.usuarioID)).subscribe(
        res => this.rotasService.verUsuario(res.id)
      );
    } else {
      this.saveUsuarioSubscription = this.usuarioService
      .save(this.getUsuario(this.usuarioID)).subscribe(
        res => this.rotasService.verUsuario(res.id)
      );
    }
  }

  getUsuario(id: number) {
    return {
      id: id,
      nome: this.formGroup.get('nome').value,
      username: this.formGroup.get('username').value,
      email: this.formGroup.get('email').value,
      numeroDeTelefone: this.formGroup.get('numeroDeTelefone').value,
      password: this.formGroup.get('password').value
    };
  }

  listUsuario() {
    this.rotasService.listUsuario();
  }

  home() {
    this.rotasService.home();
  }

  isVisiblePassword() {
    if (this.visiblePassword === 'password') {
      this.visiblePassword = 'text';
    } else {
      this.visiblePassword = 'password';
    }
  }

  ngOnDestroy() {
    if (this.idSubscription != null) {
      this.idSubscription.unsubscribe();
    }
    if (this.usuarioSubscription != null) {
      this.usuarioSubscription.unsubscribe();
    }
    if (this.saveUsuarioSubscription != null) {
      this.saveUsuarioSubscription.unsubscribe();
    }
    if (this.updateUsuarioSubscription != null) {
      this.updateUsuarioSubscription.unsubscribe();
    }
  }

}
