import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { ConfirmDialogModule } from 'primeng/confirmdialog';

import { UsuarioComponent } from './usuario.component';
import { VerComponent } from './ver/ver.component';
import { FormComponent } from './form/form.component';
import { UsuarioRoutingModule } from './usuario-routing.module';

@NgModule({
  declarations: [UsuarioComponent, VerComponent, FormComponent],
  imports: [
    CommonModule,
    UsuarioRoutingModule,
    HttpClientModule,
    RouterModule,
    ReactiveFormsModule,
    TableModule,
    ButtonModule,
    ConfirmDialogModule
  ]
})
export class UsuarioModule { }
