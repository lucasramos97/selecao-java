import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Usuario } from './model/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private readonly URL_BASE = 'http://localhost:8080/usuario';

  constructor(private httpClient: HttpClient) { }

  getAll() {
    return this.httpClient.get<Usuario[]>(this.URL_BASE);
  }

  getById(id: number) {
    return this.httpClient.get<Usuario>(`${this.URL_BASE}/${id}`);
  }

  save(usuario: Usuario) {
    return this.httpClient.post<Usuario>(this.URL_BASE, usuario);
  }

  update(usuario: Usuario) {
    return this.httpClient.put<Usuario>(this.URL_BASE, usuario);
  }

  delete(id: number) {
    return this.httpClient.delete(`${this.URL_BASE}/${id}`);
  }

}
