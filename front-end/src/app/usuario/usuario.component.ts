import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { ConfirmationService } from 'primeng/api';

import { Usuario } from './model/usuario';
import { UsuarioService } from './usuario.service';
import { RotasService } from './../rotas.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit, OnDestroy {

  private usuarios: Usuario[];

  private getAllSubscription: Subscription;
  private deleteSubscription: Subscription;

  constructor(
    private usuarioService: UsuarioService,
    private confirmationService: ConfirmationService,
    private rotasService: RotasService
    ) { }

  ngOnInit() {
    this.getAllSubscription = this.usuarioService.getAll().subscribe(dados => this.usuarios = dados);
  }

  home() {
    this.rotasService.home();
  }

  addUsuario() {
    this.rotasService.addUsuario();
  }

  verUsuario(id: number) {
    this.rotasService.verUsuario(id);
  }

  listUsuario() {
    this.rotasService.listUsuario();
  }

  editarUsuario(id: number) {
    this.rotasService.editarUsuario(id);
  }

  deleteUsuario(id: number) {
    this.confirmationService.confirm({
      key: 'confirmDialogDeleteUsuario',
      message: 'Tem certeza que deseja remover esse Usuario?',
      accept: () => {
        this.deleteSubscription = this.usuarioService.delete(id).subscribe(
          res => {
            alert('Usuário removido com sucesso!');
            this.ngOnInit();
          }
        );
      }
    });
  }

  ngOnDestroy() {
    if (this.getAllSubscription != null) {
      this.getAllSubscription.unsubscribe();
    }
    if (this.deleteSubscription != null) {
      this.deleteSubscription.unsubscribe();
    }
  }

}
