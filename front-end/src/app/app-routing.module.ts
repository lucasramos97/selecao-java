import { HomeComponent } from './home/home.component';
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'usuario', loadChildren: () => import('./usuario/usuario.module').then(mod => mod.UsuarioModule) },
  { path: 'combustivel', loadChildren: () => import('./combustivel/combustivel.module').then(mod => mod.CombustivelModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
