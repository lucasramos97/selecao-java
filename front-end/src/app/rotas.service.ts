import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RotasService {

  constructor(
    private router: Router
  ) { }

  home() {
    this.router.navigate(['/']);
  }

  addCombustivel() {
    this.router.navigate(['/combustivel/form/']);
  }

  addUsuario() {
    this.router.navigate(['/usuario/form/']);
  }

  verCombustivel(id: number) {
    this.router.navigate([`/combustivel/${id}`]);
  }

  verUsuario(id: number) {
    this.router.navigate([`/usuario/${id}`]);
  }

  listCombustivel() {
    this.router.navigate(['/combustivel']);
  }

  listUsuario() {
    this.router.navigate(['/usuario']);
  }

  editarCombustivel(id: number) {
    this.router.navigate([`/combustivel/form/${id}`]);
  }

  editarUsuario(id: number) {
    this.router.navigate([`/usuario/form/${id}`]);
  }

}
