import { RotasService } from './../../rotas.service';
import { Combustivel } from './../model/combustivel';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { CombustivelService } from './../combustivel.service';

@Component({
  selector: 'app-ver',
  templateUrl: './ver.component.html',
  styleUrls: ['./ver.component.css']
})
export class VerComponent implements OnInit, OnDestroy {

  private combustivel: Combustivel;

  private idSubscription: Subscription;
  private combustivelSubscription: Subscription;

  constructor(
    private combustivelService: CombustivelService,
    private activatedRoute: ActivatedRoute,
    private rotasService: RotasService
  ) { }

  ngOnInit() {
    this.idSubscription = this.activatedRoute.params.subscribe(
      params => {
        if (params.id != null) {
          this.combustivelSubscription = this.combustivelService.getById(params.id).subscribe(
            value => this.combustivel = value
          );
        }
      }
    );
  }

  listCombustivel() {
    this.rotasService.listCombustivel();
  }

  addCombustivel() {
    this.rotasService.addCombustivel();
  }

  editarCombustivel(id: number) {
    this.rotasService.editarCombustivel(id);
  }

  home() {
    this.rotasService.home();
  }

  ngOnDestroy() {
    if (this.idSubscription != null) {
      this.idSubscription.unsubscribe();
    }
    if (this.combustivelSubscription != null) {
      this.combustivelSubscription.unsubscribe();
    }
  }

}
