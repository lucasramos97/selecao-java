import { RotasService } from './../../rotas.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';

import { Combustivel } from './../model/combustivel';
import { CombustivelService } from './../combustivel.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit, OnDestroy {

  private combustivel: Combustivel;
  private formGroup: FormGroup;
  private combustivelID: number;

  private idSubscription: Subscription;
  private combustivelSubscription: Subscription;
  private saveCombustivelSubscription: Subscription;
  private updateCombustivelSubscription: Subscription;

  constructor(
    private combustivelService: CombustivelService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private rotasService: RotasService
  ) { }

  ngOnInit() {
    this.idSubscription = this.activatedRoute.params.subscribe(
      params => {
        if (params.id != null) {
          this.combustivelSubscription = this.combustivelService.getById(params.id).subscribe(
            value => {
              this.combustivelID = value.id;
              this.combustivel = value;
              this.formGroup = this.formEdit();
          });
        } else {
          this.combustivelID = 0;
          this.formGroup = this.formClear();
        }
      }
    );
  }

  formEdit() {
    return this.formBuilder.group({
      regiao: [this.combustivel.regiao],
      siglaEstado: [this.combustivel.siglaEstado],
      municipio: [this.combustivel.municipio],
      revenda: [this.combustivel.revenda],
      codigo: [this.combustivel.codigo],
      produto: [this.combustivel.produto],
      dataColeta: [this.combustivel.dataColeta],
      valorCompra: [this.combustivel.valorCompra],
      valorVenda: [this.combustivel.valorVenda],
      unidadeDeMedida: [this.combustivel.unidadeDeMedida],
      bandeira: [this.combustivel.bandeira]
    });
  }

  formClear() {
    return this.formBuilder.group({
      regiao: [null],
      siglaEstado: [null],
      municipio: [null],
      revenda: [null],
      codigo: [null],
      produto: [null],
      dataColeta: [null],
      valorCompra: [null],
      valorVenda: [null],
      unidadeDeMedida: [null],
      bandeira: [null]
    });
  }

  onSubmit() {
    if (this.combustivelID !== 0) {
      this.updateCombustivelSubscription = this.combustivelService
      .update(this.getCombustivel(this.combustivelID)).subscribe(
        res => this.rotasService.verCombustivel(res.id)
      );
    } else {
      this.saveCombustivelSubscription = this.combustivelService
      .save(this.getCombustivel(this.combustivelID)).subscribe(
        res => this.rotasService.verCombustivel(res.id)
      );
    }
  }

  getCombustivel(id: number) {
    return {
      id: id,
      regiao: this.formGroup.get('regiao').value,
      siglaEstado: this.formGroup.get('siglaEstado').value,
      municipio: this.formGroup.get('municipio').value,
      revenda: this.formGroup.get('revenda').value,
      codigo: this.formGroup.get('codigo').value,
      produto: this.formGroup.get('produto').value,
      dataColeta: this.formGroup.get('dataColeta').value,
      valorCompra: this.formGroup.get('valorCompra').value,
      valorVenda: this.formGroup.get('valorVenda').value,
      unidadeDeMedida: this.formGroup.get('unidadeDeMedida').value,
      bandeira: this.formGroup.get('bandeira').value
    };
  }

  listCombustivel() {
    this.rotasService.listCombustivel();
  }

  home() {
    this.rotasService.home();
  }

  ngOnDestroy() {
    if (this.idSubscription != null) {
      this.idSubscription.unsubscribe();
    }
    if (this.combustivelSubscription != null) {
      this.combustivelSubscription.unsubscribe();
    }
    if (this.saveCombustivelSubscription != null) {
      this.saveCombustivelSubscription.unsubscribe();
    }
    if (this.updateCombustivelSubscription != null) {
      this.updateCombustivelSubscription.unsubscribe();
    }
  }

}
