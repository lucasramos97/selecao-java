import { RotasService } from './../../rotas.service';
import { Combustivel } from './../model/combustivel';
import { Subscription } from 'rxjs';
import { CombustivelService } from './../combustivel.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-operacoes',
  templateUrl: './operacoes.component.html',
  styleUrls: ['./operacoes.component.css']
})
export class OperacoesComponent implements OnInit, OnDestroy {

  private combustiveis: Combustivel[];

  private regiao: string;
  private distribuidora: string;
  private dataColeta: string;

  private municipioMedia: string;
  private municipioCompraVenda: string;
  private bandeiraCompraVenda: string;

  private mediaCombustivelMunicipio: number;
  private mediaCombustivelMunicipioCompraVenda: any;
  private mediaCombustivelBandeiraCompraVenda: any;

  private mediaCombustivelMunicipioSubscription: Subscription;
  private mediaCombustivelMunicipioCompraVendaSubscription: Subscription;
  private mediaCombustivelBandeiraCompraVendaSubscription: Subscription;
  private buscarCombustiveisPorRegiaoSubscription: Subscription;
  private buscarCombustiveisPorDistribuidoraSubscription: Subscription;
  private buscarCombustiveisPorDataColetaSubscription: Subscription;

  constructor(
    private combustivelService: CombustivelService,
    private rotasService: RotasService
  ) { }

  ngOnInit() {
  }

  setMunicipioMedia(event: any) {
    this.municipioMedia = event.target.value;
  }

  setMunicipioCompraVenda(event: any) {
    this.municipioCompraVenda = event.target.value;
  }

  setBandeiraCompraVenda(event: any) {
    this.bandeiraCompraVenda = event.target.value;
  }

  setRegiao(event: any) {
    this.regiao = event.target.value;
  }

  setDistribuidora(event: any) {
    this.distribuidora = event.target.value;
  }

  setDataColeta(event: any) {
    this.dataColeta = event.target.value;
  }

  buscarCombustiveisPorRegiao() {
    this.buscarCombustiveisPorRegiaoSubscription = this.combustivelService.buscarCombustiveisPorRegiao(this.regiao)
    .subscribe(
      res => this.combustiveis = res
    );
  }

  buscarCombustiveisPorDistribuidora() {
    this.buscarCombustiveisPorDistribuidoraSubscription = this.combustivelService.buscarCombustiveisPorDistribuidora(this.distribuidora)
    .subscribe(
      res => this.combustiveis = res
    );
  }

  buscarCombustiveisPorDataColeta() {
    this.buscarCombustiveisPorDataColetaSubscription = this.combustivelService.buscarCombustiveisPorDataColeta(this.dataColeta)
    .subscribe(
      res => this.combustiveis = res,
      () => alert('Informe a data no padrão: dd-MM-yyyy')
    );
  }

  calcularMediaCombustivelMunicipio() {
    this.mediaCombustivelMunicipioSubscription = this.combustivelService.getMediaCombustivelMunicipio(this.municipioMedia)
    .subscribe(
      res => this.mediaCombustivelMunicipio = res,
      () => this.error()
    );
  }

  calcularMediaCombustivelCompraVendaMunicipio() {
    this.mediaCombustivelMunicipioCompraVendaSubscription = this.combustivelService
    .getMediaCombustivelMunicipioCompraVenda(this.municipioCompraVenda).subscribe(
      res => this.mediaCombustivelMunicipioCompraVenda = res,
      () => this.error()
    );
  }

  calcularMediaCombustivelCompraVendaBandeira() {
    this.mediaCombustivelBandeiraCompraVendaSubscription = this.combustivelService
    .getMediaCombustivelBandeiraCompraVenda(this.bandeiraCompraVenda).subscribe(
      res => this.mediaCombustivelBandeiraCompraVenda = res,
      () => this.error()
    );
  }

  error() {
    alert('Nenhum resgistro encontrado!');
  }

  home() {
    this.rotasService.home();
  }

  ngOnDestroy() {
    if (this.mediaCombustivelMunicipioSubscription != null) {
      this.mediaCombustivelMunicipioSubscription.unsubscribe();
    }
    if (this.mediaCombustivelMunicipioCompraVendaSubscription != null) {
      this.mediaCombustivelMunicipioCompraVendaSubscription.unsubscribe();
    }
    if (this.mediaCombustivelBandeiraCompraVendaSubscription != null) {
      this.mediaCombustivelBandeiraCompraVendaSubscription.unsubscribe();
    }
    if (this.buscarCombustiveisPorRegiaoSubscription != null) {
      this.buscarCombustiveisPorRegiaoSubscription.unsubscribe();
    }
    if (this.buscarCombustiveisPorDistribuidoraSubscription != null) {
      this.buscarCombustiveisPorDistribuidoraSubscription.unsubscribe();
    }
    if (this.buscarCombustiveisPorDataColetaSubscription != null) {
      this.buscarCombustiveisPorDataColetaSubscription.unsubscribe();
    }
  }

}
