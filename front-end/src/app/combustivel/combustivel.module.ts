import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import {ConfirmDialogModule} from 'primeng/confirmdialog';

import { CombustivelComponent } from './combustivel.component';
import { VerComponent } from './ver/ver.component';
import { FormComponent } from './form/form.component';
import { CombustivelRoutingModule } from './combustivel-routing.module';
import { OperacoesComponent } from './operacoes/operacoes.component';

@NgModule({
  declarations: [CombustivelComponent, VerComponent, FormComponent, OperacoesComponent],
  imports: [
    CommonModule,
    CombustivelRoutingModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    TableModule,
    ButtonModule,
    ConfirmDialogModule
  ]
})
export class CombustivelModule { }
