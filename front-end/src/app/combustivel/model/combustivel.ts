
export interface Combustivel {
  id: number;
  regiao: string;
  siglaEstado: string;
  municipio: string;
  revenda: string;
  codigo: string;
  produto: string;
  dataColeta: string;
  valorCompra: number;
  valorVenda: number;
  unidadeDeMedida: string;
  bandeira: string;
}
