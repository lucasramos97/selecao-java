import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CombustivelComponent } from './combustivel.component';
import { VerComponent } from './ver/ver.component';
import { FormComponent } from './form/form.component';
import { OperacoesComponent } from './operacoes/operacoes.component';

const routes: Routes = [
  { path: '', component: CombustivelComponent },
  { path: 'operacoes', component: OperacoesComponent },
  { path: 'form', component: FormComponent },
  { path: 'form/:id', component: FormComponent },
  { path: ':id', component: VerComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CombustivelRoutingModule { }
