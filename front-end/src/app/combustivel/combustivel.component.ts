import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { ConfirmationService } from 'primeng/api';

import { Combustivel } from './model/combustivel';
import { CombustivelService } from './combustivel.service';
import { RotasService } from './../rotas.service';

@Component({
  selector: 'app-combustivel',
  templateUrl: './combustivel.component.html',
  styleUrls: ['./combustivel.component.css']
})
export class CombustivelComponent implements OnInit, OnDestroy {

  private combustiveis: Combustivel[];

  private getAllSubscription: Subscription;
  private deleteSubscription: Subscription;

  constructor(
    private combustivelService: CombustivelService,
    private confirmationService: ConfirmationService,
    private rotasService: RotasService
    ) { }

  ngOnInit() {
    this.getAllSubscription = this.combustivelService.getAll().subscribe(dados => this.combustiveis = dados);
  }

  home() {
    this.rotasService.home();
  }

  addCombustivel() {
    this.rotasService.addCombustivel();
  }

  verCombustivel(id: number) {
    this.rotasService.verCombustivel(id);
  }

  listCombustivel() {
    this.rotasService.listCombustivel();
  }

  editarCombustivel(id: number) {
    this.rotasService.editarCombustivel(id);
  }

  deleteCombustivel(id: number) {
    this.confirmationService.confirm({
      key: 'confirmDialogDeleteCombustivel',
      message: 'Tem certeza que deseja remover esse combustível?',
      accept: () => {
        this.deleteSubscription = this.combustivelService.delete(id).subscribe(
          res => {
            alert('Combustível removido com sucesso!');
            this.ngOnInit();
          }
        );
      }
    });
  }

  ngOnDestroy() {
    if (this.getAllSubscription != null) {
      this.getAllSubscription.unsubscribe();
    }
    if (this.deleteSubscription != null) {
      this.deleteSubscription.unsubscribe();
    }
  }

}
