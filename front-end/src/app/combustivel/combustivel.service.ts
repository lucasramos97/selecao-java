import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Combustivel } from './model/combustivel';

@Injectable({
  providedIn: 'root'
})
export class CombustivelService {

  private readonly URL_BASE = 'http://localhost:8080/combustivel';

  constructor(private httpClient: HttpClient) { }

  getAll() {
    return this.httpClient.get<Combustivel[]>(this.URL_BASE);
  }

  getById(id: number) {
    return this.httpClient.get<Combustivel>(`${this.URL_BASE}/${id}`);
  }

  save(combustivel: Combustivel) {
    return this.httpClient.post<Combustivel>(this.URL_BASE, combustivel);
  }

  update(combustivel: Combustivel) {
    return this.httpClient.put<Combustivel>(this.URL_BASE, combustivel);
  }

  delete(id: number) {
    return this.httpClient.delete(`${this.URL_BASE}/${id}`);
  }

  getMediaCombustivelMunicipio(municipio: string) {
    return this.httpClient.get<number>(`${this.URL_BASE}/media-municipio/${municipio}`);
  }

  getMediaCombustivelMunicipioCompraVenda(municipio: string) {
    return this.httpClient.get<any>(`${this.URL_BASE}/valor-medio-compra-venda-municipio/${municipio}`);
  }

  getMediaCombustivelBandeiraCompraVenda(bandeira: string) {
    return this.httpClient.get<any>(`${this.URL_BASE}/valor-medio-compra-venda-bandeira/${bandeira}`);
  }

  buscarCombustiveisPorRegiao(regiao: string) {
    return this.httpClient.get<Combustivel[]>(`${this.URL_BASE}/regiao/${regiao}`);
  }

  buscarCombustiveisPorDistribuidora(distribuidora: string) {
    return this.httpClient.get<Combustivel[]>(`${this.URL_BASE}/distribuidora/${distribuidora}`);
  }

  buscarCombustiveisPorDataColeta(dataColeta: string) {
    return this.httpClient.get<Combustivel[]>(`${this.URL_BASE}/data-coleta/${dataColeta}`);
  }

}
