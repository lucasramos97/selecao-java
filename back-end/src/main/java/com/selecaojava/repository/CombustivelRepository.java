package com.selecaojava.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.selecaojava.model.Combustivel;

public interface CombustivelRepository extends JpaRepository<Combustivel, Long> {

	public List<Combustivel> findAllByMunicipio(String municipio);
	public List<Combustivel> findAllByRegiao(String regiao);
	public List<Combustivel> findAllByRevenda(String revenda);
	public List<Combustivel> findAllByDataColeta(String dataColeta);
	public List<Combustivel> findAllByBandeira(String bandeira);
	
}
