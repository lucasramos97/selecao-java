package com.selecaojava.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.selecaojava.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
