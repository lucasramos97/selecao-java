package com.selecaojava.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Swagger2Config {
	
	@Bean
    public Docket combustivelAPI() {
        return new Docket(DocumentationType.SWAGGER_2)
        		.groupName("Combustível")
        		.select()
        		.apis(RequestHandlerSelectors.basePackage("com.selecaojava.controller"))
        		.paths(PathSelectors.regex("/combustivel.*"))
        		.build().apiInfo(apiEndPointsInfo())
        		.tags(new Tag("Combustível Management System", "Operações pertinentes para combustível no Combustível Management System."));
    }
	
	@Bean
    public Docket usuarioAPI() {
        return new Docket(DocumentationType.SWAGGER_2)
        		.groupName("Usuário")
        		.select()
        		.apis(RequestHandlerSelectors.basePackage("com.selecaojava.controller"))
        		.paths(PathSelectors.regex("/usuario.*"))
        		.build().apiInfo(apiEndPointsInfo())
        		.tags(new Tag("Usuário Management System", "Operações pertinentes para usuário no Usuário Management System."));
    }
	
    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("Spring Boot REST API")
            .description("Seleção Java Management REST API")
            .contact(new Contact("Lucas Ramos", "https://www.linkedin.com/in/lucasramos97/", "lucasramosdev@gmail.com"))
            .license("Apache 2.0")
            .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
            .version("1.0.0")
            .build();
    }
	
}
