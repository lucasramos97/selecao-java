package com.selecaojava;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.selecaojava.repository.CombustivelRepository;
import com.selecaojava.util.CSVUtil;

@SpringBootApplication
public class BackEndApplication {
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(BackEndApplication.class, args);
	}
	
	@Bean
    ApplicationRunner init(CombustivelRepository repository) {
        return args -> {
        	repository.saveAll(CSVUtil.getCombustiveisFromCSV());
        };
    }

}
