package com.selecaojava.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@ApiModel(description = "Todos os detalhes do Usuário.")
public class Usuario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(notes = "ID do Usuário gerado automaticamente pelo banco de dados.")
	private long id;
	
	@NotEmpty
	@Column(length = 50)
	@ApiModelProperty(notes = "Nome do Usuário.")
	private String nome;
	
	@NotEmpty
	@Column(length = 50)
	@ApiModelProperty(notes = "Username do Usuário.")
	private String username;
	
	@NotEmpty
	@Column(length = 50)
	@ApiModelProperty(notes = "Password do Usuário.")
	private String password;
	
	@NotEmpty
	@Column(length = 50)
	@Email
	@ApiModelProperty(notes = "E-Mail do Usuário.")
	private String email;
	
	@NotEmpty
	@Column(length = 50)
	@ApiModelProperty(notes = "Número de telefone do Usuário.")
	private String numeroDeTelefone;
	
}
