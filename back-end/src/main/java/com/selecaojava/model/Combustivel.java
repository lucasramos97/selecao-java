package com.selecaojava.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @ToString
@ApiModel(description = "Todos os detalhes do Combustível.")
public class Combustivel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(notes = "ID do Combustível gerado automaticamente pelo banco de dados.")
	private long id;
	
	@NotEmpty
	@Column(length = 2)
	@ApiModelProperty(notes = "Região do Combustível.")
	private String regiao;
	
	@NotEmpty
	@Column(length = 2)
	@ApiModelProperty(notes = "Sigla do Estado do Combustível.")
	private String siglaEstado;
	
	@NotEmpty
	@Column(length = 100)
	@ApiModelProperty(notes = "Municipio do Combustível.")
	private String municipio;
	
	@NotEmpty
	@Column(length = 100)
	@ApiModelProperty(notes = "Empresa que efetua a revenda do Combustível.")
	private String revenda;
	
	@NotEmpty
	@Column(length = 20)
	@ApiModelProperty(notes = "Código do Combustível.")
	private String codigo;
	
	@NotEmpty
	@Column(length = 20)
	@ApiModelProperty(notes = "Tipo específico do Combustível.")
	private String produto;
	
	@NotEmpty
	@Column(length = 11)
	@ApiModelProperty(notes = "Data da coleta do Combustível.")
	private String dataColeta;
	
	@NotNull
	@ApiModelProperty(notes = "Valor da compra do Combustível.")
	private double valorCompra;
	
	@NotNull
	@ApiModelProperty(notes = "Valor de venda do Combustível.")
	private double valorVenda;
	
	@NotEmpty
	@Column(length = 20)
	@ApiModelProperty(notes = "Unidade de medida do Combustível.")
	private String unidadeDeMedida;
	
	@NotEmpty
	@Column(length = 100)
	@ApiModelProperty(notes = "Bandeira do Combustível.")
	private String bandeira;
	
}
