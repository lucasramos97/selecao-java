package com.selecaojava.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.core.io.ClassPathResource;

import com.selecaojava.model.Combustivel;

public class CSVUtil {

	public static List<Combustivel> getCombustiveisFromCSV() throws IOException {
		
		try {
			
			List<Combustivel> combustiveis = new ArrayList<Combustivel>();
			String regiao = "";
			String siglaEstado = "";
			String municipio = "";
			String revenda = "";
			String codigo = "";
			String produto = "";
			String dataColeta = "";
			String valorCompra = "";
			String valorVenda = "";
			String unidadeDeMedida = "";
			String bandeira = "";
			
			File file = new ClassPathResource("file.csv").getFile();
			Scanner reader = new Scanner(file);
			reader.nextLine();
			while (reader.hasNextLine()) {
				String[] data = reader.nextLine().split("  ");
				regiao = data[0].trim().replaceAll("\\s+", " ");
				siglaEstado = data[1].trim().replaceAll("\\s+", " ");
				municipio = data[2].trim().replaceAll("\\s+", " ");
				revenda = data[3].trim().replaceAll("\\s+", " ");
				codigo = data[4].trim().replaceAll("\\s+", " ");
				produto = data[5].trim().replaceAll("\\s+", " ");
				dataColeta = data[6].trim().replaceAll("\\s+", " ");
				valorCompra = data[7];
				
				if (valorCompra.isEmpty()) {
					valorCompra = data[8].replace(",", ".");
					valorVenda = "0";
				} else {
					valorCompra = data[7].replace(",", ".");
					valorVenda = data[8].replace(",", ".");
				}
				unidadeDeMedida = data[9].trim().replaceAll("\\s+", " ").split(" / ")[1];
				bandeira = data[10].trim().replaceAll("\\s+", " ");;
				
				Combustivel combustivel = new Combustivel();
				combustivel.setRegiao(regiao);
				combustivel.setSiglaEstado(siglaEstado);
				combustivel.setMunicipio(municipio);
				combustivel.setRevenda(revenda);
				combustivel.setCodigo(codigo);
				combustivel.setProduto(produto);
				combustivel.setDataColeta(dataColeta);
				combustivel.setValorCompra(Double.valueOf(valorCompra));
				combustivel.setValorVenda(Double.valueOf(valorVenda));
				combustivel.setUnidadeDeMedida(unidadeDeMedida);
				combustivel.setBandeira(bandeira);
				combustiveis.add(combustivel);
			}
			reader.close();
			return combustiveis;
	    } catch (FileNotFoundException e) {
	    	System.out.println("An error occurred.");
	    	e.printStackTrace();
	    }
		return null;
	}
	
}
