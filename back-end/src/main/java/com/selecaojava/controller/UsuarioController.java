package com.selecaojava.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.selecaojava.exception.ResourceNotFoundException;
import com.selecaojava.model.Usuario;
import com.selecaojava.repository.UsuarioRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("usuario")
@Api(tags = "Usuário Management System")
public class UsuarioController {
	
	@Autowired
	private UsuarioRepository repository;
	
	@ApiOperation(value = "Busca todos os Usuários.", response = List.class)
	@GetMapping
	public List<Usuario> getAll() {
		return this.repository.findAll();
	}
	
	@ApiOperation(value = "Busca um Usuário através do seu ID.")
	@GetMapping("{id}")
	public ResponseEntity<?> getById(@ApiParam(value = "ID do Usuário para efetuar a busca.", required = true) @PathVariable long id) throws ResourceNotFoundException {
		Usuario usuario = this.repository.findById(id)
		.orElseThrow(() -> new ResourceNotFoundException(getMessageUsuarioNotFoundById(id)));
		return ResponseEntity.ok(usuario);
	}
	
	@ApiOperation(value = "Cadastra um Usuário no banco de dados.")
	@PostMapping
	public ResponseEntity<?> save(@ApiParam(value = "Objeto Usuário que será adicionado ao banco de dados.", required = true) @Valid @RequestBody Usuario usuario) {
		return new ResponseEntity<>(this.repository.save(usuario), HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Atualiza um Usuário no banco de dados.", response = Usuario.class)
	@PutMapping
	public Usuario update(@ApiParam(value = "Objeto Usuário que será atualizado no banco de dados.", required = true) @Valid @RequestBody Usuario usuario) {
		return this.repository.save(usuario);
	}
	
	@ApiOperation(value = "Deleta um Usuário do banco de dados.")
	@DeleteMapping("{id}")
	public ResponseEntity<?> delete(@ApiParam(value = "ID do Usuário para efetuar a remoção.", required = true) @PathVariable long id) throws ResourceNotFoundException {
		Usuario usuario = this.repository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(getMessageUsuarioNotFoundById(id)));
		this.repository.delete(usuario);
		return ResponseEntity.ok().build();
	}
	
	private String getMessageUsuarioNotFoundById(long id) {
		return "Usuário não encontrado com ID: " + id;
	}
	
}
