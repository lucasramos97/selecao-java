package com.selecaojava.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.selecaojava.exception.ResourceNotFoundException;
import com.selecaojava.model.Combustivel;
import com.selecaojava.repository.CombustivelRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("combustivel")
@Api(tags = "Combustível Management System")
public class CombustivelController {
	
	@Autowired
	private CombustivelRepository repository;
	
	@ApiOperation(value = "Busca todos os Combustíveis.", response = List.class)
	@GetMapping
	public List<Combustivel> getAll() {
		return this.repository.findAll();
	}
	
	@ApiOperation(value = "Busca um Combustível através do seu ID.", response = Combustivel.class)
	@GetMapping("{id}")
	public ResponseEntity<?> getById(@ApiParam(value = "ID do Combustível para efetuar a busca.", required = true) @PathVariable long id) throws ResourceNotFoundException {
		Combustivel combustivel = this.repository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(getMessageCombustivelNotFoundById(id)));
		return ResponseEntity.ok(combustivel);
	}
	
	@ApiOperation(value = "Cadastra um Combustível no banco de dados.", response = Combustivel.class)
	@PostMapping
	public ResponseEntity<?> save(@ApiParam(value = "Objeto Combustível que será adicionado ao banco de dados.", required = true) @Valid @RequestBody Combustivel combustivel) {
		return new ResponseEntity<>(this.repository.save(combustivel), HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Atualiza um Combustível no banco de dados.", response = Combustivel.class)
	@PutMapping
	public Combustivel update(@ApiParam(value = "Objeto Combustível que será atualizado no banco de dados.", required = true) @Valid @RequestBody Combustivel combustivel) {
		return this.repository.save(combustivel);
	}
	
	@ApiOperation(value = "Deleta um Combustível do banco de dados.", response = Combustivel.class)
	@DeleteMapping("{id}")
	public ResponseEntity<?> delete(@ApiParam(value = "ID do Combustível para efetuar a remoção.", required = true) @PathVariable long id) throws ResourceNotFoundException {
		Combustivel combustivel = this.repository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(getMessageCombustivelNotFoundById(id)));
		this.repository.delete(combustivel);
		return ResponseEntity.ok().build();
	}
	
	private String getMessageCombustivelNotFoundById(long id) {
		return "Combustível não encontrado com ID: " + id;
	}
	
	@ApiOperation(value = "Efetua o calculo da média de preço de combustível com base no nome do município.", response = Double.class)
	@GetMapping("media-municipio/{municipio}")
	public ResponseEntity<?> getMediaPrecoPorMunicipio(@ApiParam(value = "Nome do município do qual será efetuado o calculo.", required = true) @PathVariable String municipio) {
		double media = 0;
		List<Combustivel> combustiveis = this.repository.findAllByMunicipio(municipio);
		if (combustiveis.size() == 0) {
			return ResponseEntity.badRequest().body("Nenhum registro encontrado!");
		}
		for (Combustivel c: combustiveis) {
			media += c.getValorCompra();
		}
		return ResponseEntity.ok(media / combustiveis.size());
	}
	
	@ApiOperation(value = "Busca todos os Combustíveis filtrados pela região.", response = List.class)
	@GetMapping("regiao/{regiao}")
	public ResponseEntity<?> getAllByRegiao(@ApiParam(value = "Nome da região utilizado para filtrar a busca.", required = true) @PathVariable String regiao) {
		List<Combustivel> combustiveis = this.repository.findAllByRegiao(regiao);
		if (combustiveis.size() == 0) {
			return ResponseEntity.badRequest().body("Nenhum registro encontrado!");
		}
		return ResponseEntity.ok(combustiveis);
	}
	
	@ApiOperation(value = "Busca todos os Combustíveis filtrados pela distribuidora.", response = List.class)
	@GetMapping("distribuidora/{distribuidora}")
	public ResponseEntity<?> getAllByDistribuidora(@ApiParam(value = "Nome da distribuidora utilizado para filtrar a busca.", required = true) @PathVariable String distribuidora) {
		List<Combustivel> combustiveis = this.repository.findAllByRevenda(distribuidora);
		if (combustiveis.size() == 0) {
			return ResponseEntity.badRequest().body("Nenhum registro encontrado!");
		}
		return ResponseEntity.ok(combustiveis);
	}
	
	@ApiOperation(value = "Busca todos os Combustíveis filtrados pela data da coleta.", response = List.class)
	@GetMapping("data-coleta/{dataColeta}")
	public ResponseEntity<?> getAllByDataColeta(@ApiParam(value = "Data da coleta utilizada para filtrar a busca.", required = true) @PathVariable String dataColeta) {
		String[] data = dataColeta.split("-");
		String dataColetaBusca = data[0] + "/" + data[1] + "/" + data[2];
		List<Combustivel> combustiveis = this.repository.findAllByDataColeta(dataColetaBusca);
		if (combustiveis.size() == 0) {
			return ResponseEntity.badRequest().body("Nenhum registro encontrado!");
		}
		return ResponseEntity.ok(combustiveis);
	}
	
	@ApiOperation(value = "Efetua o calculo do valor médio do valor da compra e do valor da venda por município.", response = Map.class)
	@GetMapping("valor-medio-compra-venda-municipio/{municipio}")
	public ResponseEntity<?> getValorMedioCompraVendaByMunicipio(@ApiParam(value = "Nome do município do qual será efetuado o calculo.", required = true) @PathVariable String municipio) {
		List<Combustivel> combustiveis = this.repository.findAllByMunicipio(municipio);
		if (combustiveis.size() == 0) {
			return ResponseEntity.badRequest().body("Nenhum registro encontrado!");
		}
		double mediaCompra = 0;
		double mediaVenda = 0;
		for (Combustivel c: combustiveis) {
			if (c.getValorVenda() != 0) {
				mediaCompra += c.getValorCompra();
				mediaVenda += c.getValorVenda();
			}
		}
		Map<String, Double> response = new HashMap<String, Double>();
		response.put("valorCompra", mediaCompra/combustiveis.size());
		response.put("valorVenda", mediaVenda/combustiveis.size());
		return ResponseEntity.ok(response);
	}
	
	@ApiOperation(value = "Efetua o calculo do valor médio do valor da compra e do valor da venda por bandeira.", response = Map.class)
	@GetMapping("valor-medio-compra-venda-bandeira/{bandeira}")
	public ResponseEntity<?> getValorMedioCompraVendaByBandeira(@ApiParam(value = "Bandeira da qual será efetuado o calculo.", required = true) @PathVariable String bandeira) {
		List<Combustivel> combustiveis = this.repository.findAllByBandeira(bandeira);
		if (combustiveis.size() == 0) {
			return ResponseEntity.badRequest().body("Nenhum registro encontrado!");
		}
		double mediaCompra = 0;
		double mediaVenda = 0;
		for (Combustivel c: combustiveis) {
			if (c.getValorVenda() != 0) {
				mediaCompra += c.getValorCompra();
				mediaVenda += c.getValorVenda();
			}
		}
		Map<String, Double> response = new HashMap<String, Double>();
		response.put("valorCompra", mediaCompra/combustiveis.size());
		response.put("valorVenda", mediaVenda/combustiveis.size());
		return ResponseEntity.ok(response);
	}
	
}
